// Load express and path
var express = require("express");
var path = require("path");

// Create instance of express app
var appExpress = express();

var idx=0;

// Define routes
appExpress.use(express.static(path.join(__dirname,"public")));
// appExpress.use("/images", express.static(path.join(__dirname,"public")); // Remap

appExpress.use(function(req,resp) {
    resp.status(404);
    resp.type("text/html"); // Representation of resource
//    resp.send('<p><img src="' +"/image/cartoon"+parseInt(Math.random() * 6)+'.jpg" alt=""></p>');
    resp.send('<p><img src="' +"/image/cartoon"+ idx +'.jpg" alt=""></p>');
    idx++;
    if (idx==6) idx=0;
});

appExpress.set("port",parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000);

appExpress.listen(appExpress.get("port"), function() {
    console.info("Application is listening on port " + appExpress.get("port"));
});

